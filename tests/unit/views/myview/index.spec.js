import { shallowMount } from '@vue/test-utils'
import MyView from '@/views/myview/index.vue'

const stubs = {
  'el-table': true,
  'el-table-column': true,
}

const directives = {
  loading: jest.fn()
}

const mockedAxios = { get: jest.fn() }
const mockedFetchData = jest.fn()

describe('MyView', () => {

let wrapper

  describe('#render', () => {
    it('renders correctly', () => {
      wrapper = shallowMount(MyView, {
        stubs,
        directives,
        mocks: {
          $axios: mockedAxios
        }
      })
      expect(wrapper.element).toMatchSnapshot()
    })
  })

  describe('hooks', () => {
    describe('#created', () => {
      it('calls #fetchData', () => {
        wrapper = shallowMount(MyView, {
          stubs,
          directives,
          mocks: {
            $axios: mockedAxios
          },
          methods: {
            fetchData: mockedFetchData
          }
        })
        expect(mockedFetchData).toHaveBeenCalled()
      })
    })
  })
})
