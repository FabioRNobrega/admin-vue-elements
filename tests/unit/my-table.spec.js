import { shallowMount } from '@vue/test-utils'
import MyTable from '@/views/table/my-table.vue'

const stubs = {
  'el-input': true,
  'el-button': true,
  'el-table': true,
  'el-table-column': true,
}

const directives = {
  loading: jest.fn(),
}

describe('MyTable', () => {
  describe('#render', () => {
    it('renders correctly', () => {
      const wrapper = shallowMount(MyTable, {
        stubs,
        directives
      })
      expect(wrapper.element).toMatchSnapshot()
    })
  })
})
