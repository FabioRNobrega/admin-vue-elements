# Admin POC | Lemoney

This is one POC for better understanding of vue-element-admin librarie
This project was generated with [Vue Admin Elements](https://panjiachen.github.io/vue-element-admin-site/guide/#vue-ecosystem)

Table of contents
=================

  * [Install](#install)
  * [Usage](#usage)
  * [Tests](#tests)
  * [Important Libraries](#important-libraries)
  * [Architecture overview](#architecture-overview)

## Install 

+ Clone the repo and cd into

``` bash
$ npm install
```

## Usage 

```bash 
$ npm run dev 
```

The application will become available at the URL:

```
http://localhost:8080/
```

## Tests

This project uses the Jest for test, that you can find on the tests folder. And run by:

```
npm run test
```
## Architecture overview
This is a simple site for a better understanding of the vue-admin-element librarie.